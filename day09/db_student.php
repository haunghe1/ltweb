<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <link rel="stylesheet" href="bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <title>Database</title>
    <script>
        function confirmDeleteAction(id) {
            var result = confirm("Bạn muốn xóa sinh viên này?");
            if (result) {
                $.ajax({
                  url: 'delete.php',
                  type: 'POST',
                  data: {
                    action: 'deleteSQL',
                    arg1: id,
                  },

                  success: function(response) {
                    alert(response);
                    location.reload();
                  },
                  error: function(xhr, status, error) {
                    console.error(xhr.responseText);
                  }
                });
            }
        }

        function resetSearch() {
            document.getElementById("major").value = "0";
            document.getElementById("keyword").value = "";
        }

        function search() {
                var major = $("#major").val();
                var keyword = $('#keyword').val();
                $.ajax({
                    url: "search.php",
                    type: "POST",
                    data: {major: major, keyword: keyword},
                    dataType: "JSON",
                    success: function(response) {
                        if (response.status === "success") {
                            $('#tbody_table').html(response.row);
                            $('.number_student').html('<p>Số sinh viên tìm thấy: ' + response.num + '</p');
                        }
                    }
                });
            }

        $(document).ready(function(){
            $('#major').change(function(){
                search()
            })

            $('#keyword').keyup(function(){
                search()
            })
        })
    </script>

    <style>
    .form-control {
        background-color: #fff;
        border: 1.5px solid #1a89ba;
        display: block;
        width: 52%;
        padding: 0.3rem 0.9rem;
        font-size: 1rem;
        color: #495057;
      }

      .form-label{
        min-width: 18%;
        margin-right: 5%;
        padding: 1%;
        color: black;
    }

    .btn-primary{
        background-color: #6ca834;
    }
    </style>

</head>

<body>
    <?php
        $servername = "localhost";
        $username = "root";
        $password = "";
        $database = "ltweb";

        $conn = new mysqli($servername, $username, $password, $database);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        $query = "SELECT * FROM sinhvien";
        $result = $conn->query($query);

       $data = array();
       if ($result !== false && $result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }
        }

        $conn->close();
    ?>

    <div style="background-color: gray; z-index: -99;">
        <div class="container" style="padding: 2% 10%; background-color: white;">
            <div class="form_area" style="padding: 5% 15%; border-radius: 10px;">
                <form>
                    <div class="form-group" style="display: flex; padding: 2%; margin-left: 10%; justify-content: flex-start;">
                        <label for="khoa" class="form-label">Khoa</label>
                        <select name="major" id="major" class="form-control" >
                            <option value="0" selected>--Chọn phân khoa--</option>
                        <?php
                            $major = [
                                "MAT" => "Khoa học máy tính",
                                "KDL" => "Khoa học vật liệu",
                            ];

                            foreach ($major as $key => $value) {
                                echo '<option value="' . $key . '">' . $value . '</option>';
                            }
                        ?>
                        </select>
                    </div>
                    <div class="form-group" style="display: flex; padding: 2%; margin-left: 10%; justify-content: flex-start;">
                        <label for="keyword" class="form-label">Từ Khoá</label>
                        <input type="text" class="form-control" id="keyword">
                    </div>
                    <div class="form-group" style="display: flex; padding-top: 15px;">
                        <input type="submit" onclick="resetSearch()" class="btn btn-primary" name="btn_submit" value="Reset" style="margin: auto;">
                    </div>
                </form>
            </div>

            <div class="table_area">
                <div style="display: flex; justify-content: space-between; align-items: center;">
                    <div class="number_student">
                        <p>Số sinh viên tìm thấy: <?php echo count($data); ?></p>
                    </div>
                    <div class="add_student">
                        <a href="./register.php"><button type="button" class="btn btn-primary">Thêm</button></a>
                    </div>
                </div>
                <table class="table table-striped">
                    <thead>
                        <tr>
                        <th scope="col">No</th>
                        <th scope="col">Tên sinh viên</th>
                        <th scope="col">Khoa</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody id="tbody_table">
                        <?php
                            $major = [
                                "MAT" => "Khoa học máy tính",
                                "KDL" => "Khoa học vật liệu"
                            ];
                            $i = 0;
                            foreach ($data as $student) {
                                $i = $i + 1;
                        ?>
                        <tr>
                            <th scope="row"><?php echo $i; ?></th>
                            <td><?php echo $student['HoTen']; ?></td>
                            <td><?php echo $student['PhanKhoa']; ?></td>
                            <td>
                                <button onclick="confirmDeleteAction('<?php echo $student['MaSV']; ?>')" type="button" class="btn btn-primary">Xóa</button>
                                <a href="update_students.php?id=<?php echo $student['MaSV']; ?>"><button type="button" class="btn btn-primary">Sửa</button>
                            </td>
                        </tr>
                        <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
