<?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $database = "ltweb";

        $conn = new mysqli($servername, $username, $password, $database);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        $id = $_POST['id'];
        $hoTen = $_POST['name'];
        $gioiTinh = $_POST['gender'];
        $phanKhoa = $_POST['major'];
        $ngaySinh = date('Y-m-d', strtotime(str_replace('/', '-', $_POST['birth'])));
        $diaChi = $_POST['address'];
        $hinhAnh = $_POST['img'];


        if ("" != trim($id)) {
            if (empty($hinhAnh)) {
                $sql = "UPDATE sinhvien SET HoTen = ?, GioiTinh = ?, PhanKhoa = ?, NgaySinh = ?, DiaChi = ? WHERE MaSV = ?";
                $stmt = $conn->prepare($sql);
                $stmt->bind_param("sssssd", $hoTen, $gioiTinh, $phanKhoa, $ngaySinh, $diaChi, $id);
                $stmt->execute();
            }
            else {
                $sql = "UPDATE sinhvien SET HoTen = ?, GioiTinh = ?, PhanKhoa = ?, NgaySinh = ?, DiaChi = ?, HinhAnh = ? WHERE MaSV = ?";
                $stmt = $conn->prepare($sql);
                $stmt->bind_param("ssssssd", $hoTen, $gioiTinh, $phanKhoa, $ngaySinh, $diaChi, $hinhAnh, $id);
                $stmt->execute();
            }

            if ($stmt->affected_rows > 0)
                echo "Dữ liệu được sửa thành công!<br><br>";
            else
                echo "Dữ liệu sửa không thành công<br><br>";

            echo "<a href='db_student.php'><button type='button' class='btn btn-primary'>Quay về</button>";
        }
        else {
            $stmt = $conn->prepare("INSERT INTO SINHVIEN (HoTen, GioiTinh, PhanKhoa, NgaySinh, DiaChi, HinhAnh) VALUES (?, ?, ?, ?, ?, ?)");
            $stmt->bind_param("s", $hoTen, $gioiTinh, $phanKhoa, $ngaySinh, $diaChi, $hinhAnh);
            $stmt->execute();

            if ($stmt->affected_rows > 0) {
                echo "Successful!";
            } else {
                echo "Failed!";
                unlink($hinhAnh);
            }
        }
        

        $stmt->close();
        $conn->close();
    }
?>
