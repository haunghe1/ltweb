<?php
    if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['arg1']) && $_POST['action'] == "deleteSQL") {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $database = "ltweb";

        $conn = new mysqli($servername, $username, $password, $database);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        $id = $_POST['arg1'];

        $stmt = $conn->prepare("DELETE FROM SINHVIEN WHERE MaSV = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();

        if ($stmt->affected_rows > 0) {
            echo "Bản ghi đã được xóa thành công!";
        }
        else {
            echo "Đã có lỗi xảy ra khi thực hiện việc xóa!";
        }

        $stmt->close();
        $conn->close();
    }
    else {
        echo "Invalid request!";
    }
?>