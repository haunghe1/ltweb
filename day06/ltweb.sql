CREATE DATABASE IF NOT EXISTS ltweb;
CREATE table students (full_name varchar(255), sex bit(1), department varchar(255), date_of_birth date, address varchar(255), image blob);
