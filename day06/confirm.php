<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Xác nhận</title>
    <style>
        body {
            font-family: 'Times New Roman', Times, serif;
        }

        .outer-box {
            width: 30%;
            margin: 10% auto 0 auto;
            padding: 3rem;
        }

        .outer-box,
        .label,
        input[type=text],
        select,
        button,
        textarea#address {
            border: 2px solid #658baf;
        }

        .label {
            display: inline-block;
            width: 20%;
            padding: 7px;
            margin-bottom: 4%;
            margin-right: 20px;
            background-color:
                #70ad47;
            color: white;
            text-align: center;
        }

        #full-name,
        #department,
        #birthday {
            padding: 7px 0 7px 0;
        }

        #department,
        #birthday {
            width: 40%;
            text-align: center;
        }

        .full-name,
        .department,
        .birthday,
        .gender,
        .image,
        .address {
            display: flex;
        }

        .full-name,
        .department,
        .birthday,
        .gender,
        .address {
            align-items: baseline;
        }

        .image label {
            max-height: 1rem;
        }


        #fname-validation,
        #d-validation,
        #bd-validation,
        #bd-invalid {
            color: red;
        }

        #address {
            vertical-align: top;
        }

        button {
            font-family: inherit;
            font-size: 1rem;
            color: white;
            width: 30%;
            padding: 10px;
            background-color: #70ad47;
            border-radius: 6px;
            margin: 5% auto 0 auto;
            cursor: pointer;
        }

        .btn {
            text-align: center;
        }

        .require-star {
            color: red;
        }
    </style>
</head>

<body>
    <?php
    include 'database.php';

    $conn = openConnection();

    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

    if (isset($_POST['submit'])) {
        $full_name = $_POST['full-name'];
        $gender = $_POST['gender'];
        $department = $_POST['department'];
        $birthday = $_POST['birthday'];
        $address = $_POST['address'];

        $date_of_birth = date('Y-m-d', strtotime(str_replace('/', '-', $birthday)));
        $image_data = file_get_contents($_FILES["uploadImage"]["tmp_name"]);

        $sql = "INSERT INTO students (full_name, gender, department, date_of_birth, address, image) VALUES (?, ?, ?, ?, ?, ?)";

        $stmt = $conn->prepare($sql);
        $stmt->bind_param("sssssb", $full_name, $gender, $department, $date_of_birth, $address, $image_data);

        if (!$stmt->execute()) {
            echo "Error: " . $stmt->error;
        }

        $stmt->close();
        echo "Lưu thông tin thành công";
    }
    ?>
    <div class="outer-box">
        <form action="confirm.php" method="post">
            <div class="full-name">
                <label for="full-name" class="label">Họ tên<span class="require-star">*</span></label>
                <?php
                echo "<p>" . $_POST['full-name'] . "</p>";
                ?>
            </div>
            <div class="gender">
                <label for="gender" class="label">Giới tính<span class="require-star">*</span></label>
                <?php
                echo "<p>" . $_POST['gender'] . "</p>";
                ?>

            </div>
            <div class="department">
                <label for="department" class="label">Phân khoa<span class="require-star">*</span></label>
                <?php
                echo "<p>" . $_POST['department'] . "</p>";
                ?>
            </div>
            <div class="birthday">
                <label for="birthday" class="label">Ngày sinh<span class="require-star">*</span></label>
                <?php
                echo "<p>" . $_POST['birthday'] . "</p>";
                ?>
            </div>
            <div class="address">
                <label for="address" class="label">Địa chỉ</label>
                <?php
                echo "<p>" . $_POST['address'] . "</p>";
                ?>
            </div>
            <div class="image">
                <label for="image" class="label">Hình ảnh</label>
                <?php
                $tempFilePath = $_FILES["uploadImage"]["tmp_name"];
                $imagePath = 'Hình ảnh/' . $_FILES["uploadImage"]["name"];
                if (move_uploaded_file($tempFilePath, $imagePath)) {
                    echo "<img src=\"$imagePath\" alt=\"Uploaded Image\" width=\"200\">";
                }
                ?>
            </div>
            <div class="btn">
                <button id="btn" type="submit" value="submit" name="confirm">Xác nhận</button>
            </div>
        </form>
    </div>
</body>

</html>
