<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thông tin đăng kí</title>
</head>
<style>
    .container, body {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        height: 100vh;
        margin-top: 20px;
        background-color: white;
    }

    .container {
        width: 600px;
        border: 1px solid blue;
        color: black;
        margin: 200px auto;
    }

    .input_name {
        background-color: #70AD47;
        color: white;
        padding: 5px;
        margin: 5px;
        border-radius: 0px;
        width: 100px;
        height: 15px;
        padding-top: 9px;
        padding-bottom: 11px;
        text-align: center;
        display: inline-block;
    }

    .entering {
        color: blue;
        padding: 5px;
        border-radius: 0px;
        width: 400px;
        height: 25px;
        display: inline-block;
        margin: 5px 20px;
    }

    .fileToUpload {
        padding: 5px;
        border-radius: 0px;
        width: 400px;
        height: 25px;
        display: inline-block;
        margin: 5px 20px;
    }

    .input_gen {
        margin-bottom: 24px;
    }

    .choose_gender {
        color: black;
        padding-left: 10px;
        margin-left: 5px;
        font-size: 20px;
        display: inline-block;
    }

    .input_falcuty {
        margin-bottom: 24px;
    }

    .choose-falcuty {
        color: black;
        padding: 5px;
        margin: 5px;
        border-radius: 0px;
        width: 200px;
        height: 45px;
        padding-top: 9px;
        padding-bottom: 11px;
        text-align: center;
        display: inline-block;
        margin-left: 20px;
    }

    .button-container {
        background-color: #70AD47;
        color: white;
        padding: 5px;
        border-radius: 5px;
        width: 150px;
        height: 50px;
        margin-top: 20px;
        margin-left: 8px;
        cursor: pointer ;
        border: none;
    }

    .choose_gender label {
        display: inline-block;
        margin-right: 10px;
        margin-top: 15px;
    }

    .required {
        color: red;
    }

    .date_of_birth {
        margin-bottom: 24px;
    }

    .date_input {
        padding: 5px;
        margin: 5px;
        border-radius: 0px;
        width: 190px;
        height: 34px;
        padding-top: 0px;
        padding-bottom: 2px;
        text-align: center;
        display: inline-block;
        margin-left: 20px;
    }

    .input_address {
        border-radius: 0px;
        width: 180px;
        height: 100px;
        resize: vertical;
        padding-top: 0px;
        padding-bottom: 85px;
        display: inline-block;
        margin-left: 20px;
        padding-right: 222px;
        margin-top: 5px;
    }

    form {
        display: flex;
        align-self: flex-start;
        padding-left: 20px;
    }


    img {
        width: 200px;
        height: 200px;
    }
</style>
<body>
<div class="container">
    <div id = "errorMessage"></div>

    <form action="">
        <label for="input_name" class="input_name">Họ và tên</label>
        <p id="name"></p>
    </form>

    <form action="">
        <label for="gender" class="input_name input_gen">Giới tính</label>
        <p id="gender"></p>
    </form>

    <form action="">
        <label for="birthday" class="input_name date_of_birth">Ngày sinh </label>
        <p id="birthday"></p>
    </form>

    <form action="">
        <label for="address" class="input_name address">Địa chỉ</label>
        <p id="address"></p>
    </form>

    <form action="">
        <label for="infor" class="input_name Infor"> Thông tin khác </label>
        <p id="infor"></p>
    </form>

</div>

<script>
    document.getElementById('name').textContent = localStorage.getItem("name")
    document.getElementById('gender').textContent = localStorage.getItem("gender")
    document.getElementById('address').textContent = localStorage.getItem("address")
    document.getElementById('birthday').textContent = localStorage.getItem("birthday")

    address = localStorage.getItem("address");

    if (address !== '') {
        document.getElementById('address').textContent = address;
    }


</script>
</body>
</html>