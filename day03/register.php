<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="register.css">
    <title>Tân sinh viên</title>
    
</head>

<body>
    <div class="outer-box">
        <form action="POST">
            <div class="fullname">
                <label for="fullname" class="label">Họ tên</label>
                <input type="text" name="fullname" width="32px" height="22px" class="fullname">
            </div>
            <div class="gender">
                <label for="gender" class="label">Giới tính</label>
                <?php
                $gender = array(
                    "Nam", "Nữ"
                );

                for ($i = 0; $i < count($gender); $i++) {
                    $radio = $gender[$i];
                ?>
                    <input name="gender" type="radio" class="radio" value="<?= $radio ?>"><?= $radio ?>
                <?php } ?>

            </div>
            <div class="department">
                <label for="department" class="label">Phân khoa</label>
                <select name="department" class="department">
                    <option value="none" selected disabled hidden>--Chọn phân khoa--</option>
                    <?php
                    $departments = array(
                        "MAT" => "Khoa học máy tính",
                        "KDL" => "Khoa học vật liệu"
                    );
                    foreach ($departments as $d => $name) { ?>
                        <option value="<?= $d ?>"><?= $name ?></option>

                    <?php } ?>
                </select>
            </div>
            <div class="btn">
                <button>Đăng ký</button>
            </div>
        </form>
    </div>

</body>

</html>
