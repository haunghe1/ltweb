<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Xác nhận</title>
    <style>
	.container{
    	display: flex;
		justify-content: center;
	}
	.form-border{
	    height: 800px;
    	width: 500px;
		border: 1.5px solid #1a89ba;
    }
    .form-group{
        display: flex;
        align-items: center;
        justify-content: flex-start;
        padding: 2%;
        margin-left: 10%;
    }

    .form-label{
        background-color: #6ca834;
        border: 1.5px solid #1a89ba;
        min-width: 18%;
        margin-right: 5%;
        padding: 1%;
        color: white;
    }

    .form-control {
        background-color: #fff;
        border: 0px solid #1a89ba;
        display: block;
        width: 52%;
        padding: 0.3rem 0.9rem;
        font-size: 1rem;
        color: #495057;
      }

    .btn-submit{
        background-color: #6ca834;
        border: 1.5px solid #1a89ba;
        width: 30%;
        color: white;
        cursor: pointer;
        margin-left: 140px;
    }
    .form-group input[type="radio"] {
        width: 10%;
    }

    img {
        width: 300px;
        border: 1px solid #ccc;
    }

    </style>
</head>

<body>
    <?php
        $name = $_POST['name'];
        $gender = $_POST['gender'];
        $major_key = $_POST['major'];
        $birth = $_POST['birth'];
        $address = $_POST['address'];
        $img = md5(random_bytes(32)) . ".jpg";

        $major_value = [
             "MAT" => "Khoa học máy tính",
             "KDL" => "Khoa học vật liệu",
        ];

        if ($_SERVER['REQUEST_METHOD'] === 'POST')
            if (isset($_FILES['img']) && $_FILES['img']['error'] === UPLOAD_ERR_OK)
                move_uploaded_file($_FILES['img']['tmp_name'], $img);
            else
                $img = "None";
    ?>
    <div class="container">
        <div class="form-border">
        <div id="form-error" class="error-message"></div>
            <form action="database.php" id="form-login" method="POST">
                <div class="form-group">
                    <label for="name" class="form-label">Họ và tên</label>
                    <input type="text" name="name" class="form-control" value="<?php echo $name; ?>" readonly>
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Giới tính</label>
                    <input type="text" name="gender" class="form-control" value="<?php echo $gender; ?>" readonly>
                </div>
                <div class="form-group">
                    <label for="" class="form-label">Phân khoa</label>
                    <input type="text" name="major" class="form-control" value="<?php echo $major_value[$major_key]; ?>" readonly>
                </div>
                <div class="form-group">
                    <label for="birth" class="form-label">Ngày sinh</label>
                    <input type="text" name="birth" class="form-control" value="<?php echo $birth; ?>" readonly>

                </div>
                <div class="form-group">
                    <label for="address" class="form-label" style>Địa chỉ</label>
                    <textarea type="text" id="address" name="address" class="form-control" style="width: 223px; height: 100px; padding: 1px; margin-top:2px; font-family: Arial; padding: 0.3rem 0.9rem;" readonly><?php echo $address; ?></textarea>

                </div>
                <div class="form-group">
                    <label for="" class="form-label">Hình ảnh</label>
                    <img src="<?php echo $img; ?>">
                    <input type="text" id="img" name="img" class="form-control" value="<?php echo $img; ?>" style="display: none;">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn-submit form-control" value="Xác nhận">
                </div>
            </form>
        </div>
    </div>
</body>
</html>