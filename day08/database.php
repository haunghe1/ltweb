<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "ltweb";

try {
    $conn = new  mysqli($servername, $username, $password, $database);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } else {
        echo("Connected to database");
    }
} catch (PDOException $e) {
    die("". $e->getMessage());
}

return null;
?>