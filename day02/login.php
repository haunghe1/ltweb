<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Đăng nhập</title>
    <style>
        body {
            width: 300px;
            margin: 0 auto;
            padding: 20px 50px;
            border: 2px solid #000;
            border-radius: 10px;
            background-color: #f2f2f2; 
        }
        form {
            width: 300px;
            margin: 0 auto;
        }
        label {
            font-size: 16px;
        }
        input[type="text"],
        input[type="password"] {
            width: 100%;
            padding: 10px;
            margin: 10px 0;
            border: 1px solid #ccc;
            border-radius: 5px;
        }
        input[type="submit"] {
            background-color: #00bfff;
            color: white;
            padding: 10px 20px;
            border: none;
            border-radius: 5px;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <div>
        <?php
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $currentDateTime = date('d/m/Y H:i:s');
        echo "Bây giờ là: $currentDateTime";
        ?>
    </div>

    <form method="POST" action="process_login.php">
        <label for="username">Tên người dùng:</label><br>
        <input type="text" id="username" name="username" required><br>

        <label for="password">Mật khẩu:</label><br>
        <input type="password" id="password" name="password" required><br>

        <input type="submit" value="Đăng nhập">
    </form>
</body>
</html>
