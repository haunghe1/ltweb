<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tân sinh viên</title>
    <style>
        body {
            font-family: 'Arial', Times, serif;
        }

        .outer-box {
            width: 30%;
            margin: 10% auto 0 auto;
            padding: 3rem;
        }

        .outer-box,
        .label,
        input[type=text],
        select,
        button,
        textarea#address {
            border: 2px solid #658baf;
        }

        .label {
            display: inline-block;
            width: 20%;
            padding: 7px;
            margin-bottom: 4%;
            margin-right: 20px;
            background-color:
                    #70ad47;
            color: white;
            text-align: center;
        }

        #full-name,
        #address,
        #birthday {
            padding: 7px 0 7px 0;
        }

        #address,
        #birthday {
            width: 40%;
            text-align: center;
        }

        #fname-validation,
        #d-validation,
        #bd-validation,
        #bd-invalid {
            color: red;
        }

        #address {
            vertical-align: top;
        }

        button {
            font-family: inherit;
            font-size: 1rem;
            color: white;
            width: 30%;
            padding: 10px;
            background-color: #70ad47;
            border-radius: 6px;
            margin: 5% auto 0 auto;
            cursor: pointer;
        }

        .btn {
            text-align: center;
        }

        .require-star {
            color: red;
        }
    </style>
</head>

<body>
<div class="outer-box">
    <h2>Form đăng kí sinh viên</h2>
    <form method="post" action="confirm.php" enctype="multipart/form-data">
        <p id="fname-validation"></p>
        <p id="gender-validation"></p>
        <p id="ad-validation"></p>
        <div class="full-name">
            <label for="full-name" class="label">Họ tên</label>
            <input type="text" name="full-name" size="32" id="full-name" required>
        </div>
        <div class="gender">
            <label for="gender" class="label">Giới tính</label>
            <?php
            $gender = array(
                "Nam", "Nữ"
            );

            for ($i = 0; $i < count($gender); $i++) {
                $radio = $gender[$i];
                ?>
                <input name="gender" type="radio" class="radio" id="gender" required value="<?= $radio ?>"><?= $radio ?>
            <?php } ?>

        </div>

        <div class="birthday">
            <label for="birthday" class="label">Ngày sinh</label>
            <select id="year">
                <option value="">Năm</option>
            </select>

            <select id="month">
                <option value="">Tháng</option>
            </select>

            <select id="day">
                <option value="">Ngày</option>
            </select>


            <script>
                const daySelect = document.getElementById("day");
                const monthSelect = document.getElementById("month");
                const yearSelect = document.getElementById("year");

                for (let day = 1; day <= 31; day++) {
                    const option = document.createElement("option");
                    option.value = day;
                    option.textContent = day;
                    daySelect.appendChild(option);
                }

                for (let month = 1; month <= 12; month++) {
                    const option = document.createElement("option");
                    option.value = month;
                    option.textContent = month;
                    monthSelect.appendChild(option);
                }

                const currentYear = new Date().getFullYear();
                const minYear = currentYear - 40
                const maxYear = currentYear - 15
                for (let year = maxYear; year >= minYear; year--) {
                    const option = document.createElement("option");
                    option.value = year;
                    option.textContent = year;
                    yearSelect.appendChild(option);
                }
            </script>
        </div>
        <div class="address">
            <label for="address" class="label">Địa chỉ</label>
            <select id="city">
                <option value="">Thành phố</option>
                <option value="Hanoi">Hà Nội</option>
                <option value="HoChiMinh">Hồ Chí Minh</option>
            </select>

            <select id="district">
                <option value="">Quận</option>
            </select>

            <script>
                const citySelect = document.getElementById("city");
                const districtSelect = document.getElementById("district");


                const districtsByCity = {
                    Hanoi: ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"],
                    HoChiMinh: ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"],
                };

                citySelect.addEventListener("change", () => {
                    const selectedCity = citySelect.value;
                    const districtList = districtsByCity[selectedCity] || [];

                    districtSelect.innerHTML = '<option value="">Quận</option>';

                    districtList.forEach((district) => {
                        const option = document.createElement("option");
                        option.value = district;
                        option.textContent = district;
                        districtSelect.appendChild(option);
                    });
                });
            </script>
        </div>
        <div class="information">
            <label for="infor" class="label">Thông tin khác</label>
            <textarea id="infor" name="infor" cols=30 rows=5></textarea>
        </div>
        <div class="btn">
            <button id="btn" type="submit" value="submit" name="submit">Đăng ký</button>
        </div>
    </form>
</div>

<script>
    var btn = document.getElementById("btn");
    var fullName = document.getElementById("fname-validation");
    var birthday = document.getElementById("bd-validation");
    var gender = document.getElementById("gender-validation")

    btn.addEventListener("click", function(event) {

        if (fullName.value.trim() == "") {
            document.getElementById("fname-validation").innerHTML = "Hãy nhập họ tên";
        }

        if (fullName.value.trim() != "") {
            document.getElementById("fname-validation").innerHTML = "";
        }

        if (gender.value.trim() == "") {
            document.getElementById("bd-validation").innerHTML = "Hãy chọn giới tính";
        }

        if (gender.value.trim() != "") {
            document.getElementById("fname-validation").innerHTML = "";
        }

        if (!(dateValidation(birthday.value))) {
            document.getElementById("bd-invalid").innerHTML = "Hãy chọn ngày sinh đúng định dạng";
        } else {
            document.getElementById("bd-invalid").innerHTML = "";
        }
    });

    function dateValidation(date) {
        btn.addEventListener("click", () => {
            const selectedDay = daySelect.value;
            const selectedMonth = monthSelect.value;
            const selectedYear = yearSelect.value;

            if (!selectedDay || !selectedMonth || !selectedYear) {
                alert("Hãy chọn ngày sinh ");
            } else if (selectedMonth == 2 && selectedDay > 29) {
                alert("Tháng 2 không có nhiều hơn 29 ngày.");
            } else if (
                (selectedMonth == 4 || selectedMonth == 6 || selectedMonth == 9 || selectedMonth == 11) &&
                selectedDay > 30
            ) {
                alert("Tháng này có 30 ngày.");
            } else {
                alert("Ngày sinh hợp lệ: " + selectedDay + "/" + selectedMonth + "/" + selectedYear);
            }
    }
</script>
</body>

</html>
