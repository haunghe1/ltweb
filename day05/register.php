<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tân sinh viên</title>
    <style>
        body {
            font-family: 'Times New Roman', Times, serif;
        }

        .outer-box {
            width: 30%;
            margin: 10% auto 0 auto;
            padding: 3rem;
        }

        .outer-box,
        .label,
        input[type=text],
        select,
        button,
        textarea#address {
            border: 2px solid #658baf;
        }

        .label {
            display: inline-block;
            width: 20%;
            padding: 7px;
            margin-bottom: 4%;
            margin-right: 20px;
            background-color:
                    #70ad47;
            color: white;
            text-align: center;
        }

        #full-name,
        #department,
        #birthday {
            padding: 7px 0 7px 0;
        }

        #department,
        #birthday {
            width: 40%;
            text-align: center;
        }

        #fname-validation,
        #d-validation,
        #bd-validation,
        #bd-invalid {
            color: red;
        }

        #address {
            vertical-align: top;
        }

        button {
            font-family: inherit;
            font-size: 1rem;
            color: white;
            width: 30%;
            padding: 10px;
            background-color: #70ad47;
            border-radius: 6px;
            margin: 5% auto 0 auto;
            cursor: pointer;
        }

        .btn {
            text-align: center;
        }

        .require-star {
            color: red;
        }
    </style>
</head>

<body>
<div class="outer-box">
    <form method="post" action="confirm.php" enctype="multipart/form-data">
        <p id="fname-validation"></p>
        <p id="d-validation"></p>
        <p id="bd-validation"></p>
        <p id="bd-invalid"></p>
        <div class="full-name">
            <label for="full-name" class="label">Họ tên<span class="require-star">*</span></label>
            <input type="text" name="full-name" size="32" id="full-name" required>
        </div>
        <div class="gender">
            <label for="gender" class="label">Giới tính<span class="require-star">*</span></label>
            <?php
            $gender = array(
                "Nam", "Nữ"
            );

            for ($i = 0; $i < count($gender); $i++) {
                $radio = $gender[$i];
                ?>
                <input name="gender" type="radio" class="radio" value="<?= $radio ?>"><?= $radio ?>
            <?php } ?>

        </div>
        <div class="department">
            <label for="department" class="label">Phân khoa<span class="require-star">*</span></label>
            <select name="department" id="department">
                <option value="none" selected disabled hidden>--Chọn phân khoa--</option>
                <?php
                $departments = array(
                    "MAT" => "Khoa học máy tính",
                    "KDL" => "Khoa học vật liệu"
                );
                foreach ($departments as $d => $name) { ?>
                    <option><?= $name ?></option>

                <?php } ?>
            </select>
        </div>
        <div class="birthday">
            <label for="birthday" class="label">Ngày sinh<span class="require-star">*</span></label>
            <!-- <input type="date" id="birthday" name="birthday" placeholder="dd/mm/yyyy"> -->
            <input type="text" id="birthday" name="birthday" placeholder="dd/mm/yyyy">
        </div>
        <div class="address">
            <label for="address" class="label">Địa chỉ</label>
            <textarea id="address" name="address" cols=30 rows=5></textarea>
        </div>
        <div class="image">
            <label for="image" class="label">Hình ảnh</label>
            <input type="file" name="uploadImage">
        </div>
        <div class="btn">
            <button id="btn" type="submit" value="submit" name="submit">Đăng ký</button>
        </div>
    </form>
</div>

<script>
    var btn = document.getElementById("btn");

    var fullName = document.getElementById("full-name");

    var department = document.getElementById("department");

    var birthday = document.getElementById("birthday");

    btn.addEventListener("click", function(event) {
        // event.preventDefault();

        if (fullName.value.trim() == "") {
            document.getElementById("fname-validation").innerHTML = "Hãy nhập tên";
        }

        if (fullName.value.trim() != "") {
            document.getElementById("fname-validation").innerHTML = "";
        }

        if (department.value == "none") {
            document.getElementById("d-validation").innerHTML = "Hãy chọn phân khoa";
        }

        if (department.value != "none") {
            document.getElementById("d-validation").innerHTML = "";
        }

        if (birthday.value.trim() == "") {
            document.getElementById("bd-validation").innerHTML = "Hãy chọn ngày sinh";
        }

        if (birthday.value.trim() != "") {
            document.getElementById("bd-validation").innerHTML = "";
        }

        if (!(dateValidation(birthday.value))) {
            document.getElementById("bd-invalid").innerHTML = "Hãy nhập ngày sinh đúng định dạng";
        } else {
            document.getElementById("bd-invalid").innerHTML = "";
        }
    });

    function dateValidation(date) {
        var datePattern = /^(\d{2})\/(\d{2})\/(\d{4})$/;

        if (datePattern.test(date)) {
            var [, day, month, year] = date.match(datePattern);

            var dayInt = parseInt(day, 10);
            var monthInt = parseInt(month, 10) - 1;
            var yearInt = parseInt(year, 10);

            const dateObj = new Date(yearInt, monthInt, dayInt);

            if (
                dateObj.getDate() === dayInt &&
                dateObj.getMonth() === monthInt &&
                dateObj.getFullYear() === yearInt
            ) {
                return true;
            }
        }

        return false;
    }
</script>
</body>

</html>
