<?php
function openConnection()
{
    $host = "localhost";
    $user = "root";
    $pw = "";
    $name = "ltweb";
    $conn = new mysqli($host, $user, $pw, $name) or die("Fail connected: %s\n" . $conn->error);
    return $conn;
}
function closeConnection($connect)
{
    $connect->close();
}
