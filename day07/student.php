<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách sinh viên</title>
    <style>
        .container {
            width: 60%;
            margin: 2% auto 0 auto;
        }

        .search-box {
            display: flex;
            flex-direction: column;
        }

        label {
            display: inline-block;
            width: 10%;
            padding: 5px;
        }

        select,
        input[type="text"] {
            width: 20%;
            padding: 5px;
            margin-top: 5px;
        }

        button {
            font-family: inherit;
            font-size: 1rem;
            color: white;
            width: 10%;
            padding: 10px;
            background-color: #4f81bd;
            border-radius: 6px;
            margin: 1% auto 1% auto;
            cursor: pointer;
        }

        .btn {
            text-align: center;
        }

        .add-button {
            text-decoration: none;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        table,
        th,
        td {
            border: 1px solid black;
        }

        th,
        td {
            padding: 10px;
            text-align: left;
        }

        .action {
            border: 1px solid #6889b2;
            background-color: #92b1d6;
            padding: 3px;
            color: white;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="search-box">
            <div class="department">
                <label for="department">Khoa</label>
                <select name="depts">
                    <option value="none" selected disabled hidden>--Chọn phân khoa--</option>
                    <option value="KHMT">Khoa học máy tính</option>
                    <option value="KHVL">Khoa học vật liệu</option>
                </select>
            </div>
            <div class="search">
                <label for="search">Từ khoá</label>
                <input type="text">
            </div>
            <div class="btn">
                <button id="btn" type="submit" value="submit" name="submit">Tìm kiếm</button>
            </div>
        </div>
        <p class="no">Số sinh viên tìm thấy:
            <?php
            require('database.php');
            $ect = openConnection();
            if ($connect->connect_error) {
                die("Connection failed: " . $connect->connect_error);
            }

            $sql = "SELECT COUNT(id) FROM students";
            $result = mysqli_query($connect, $sql);
            $count = mysqli_fetch_column($result);
            echo $count;
            ?>
        </p>
        <div class="add-btn">
            <a href="../day05/register.php" class="add-button"><button>Thêm</button></a>
        </div>
        <table border="1">
            <tr>
                <th>No.</th>
                <th>Tên sinh viên</th>
                <th>Khoa</th>
                <th>Action</th>
            </tr>
            <?php
            $sql = "SELECT * FROM students";
            $result = $connect->query($sql);

            if ($result->num_rows > 0) {
                $count = 1;
                while ($row = $result->fetch_assoc()) {
                    echo "<tr>";
                    echo "<td>" . $count . "</td>";
                    echo "<td>" . $row['full_name'] . "</td>";
                    echo "<td>" . $row['department'] . "</td>";
                    echo '<td><a class="action">Xoá</a> <a class="action">Sửa</a></td>';
                    echo "</tr>";
                    $count++;
                }
            } else {
                echo "<tr><td colspan='4'>No records found.</td></tr>";
            }

            $connect->close();
            ?>
        </table>
    </div>
</body>

</html>
